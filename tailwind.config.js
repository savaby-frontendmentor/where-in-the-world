/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: 'class',
  content: ['./index.html', './src/**/*.{js,ts,jsx,tsx,vue}'],
  theme: {
    extend: {
      colors: {
        'theme-dark-blue': 'hsl(209, 23%, 22%)',
        'theme-very-dark-blue': 'hsl(207, 26%, 17%)',
        'theme-very-dark-blue-text': 'hsl(200, 15%, 8%)',
        'theme-dark-gray': 'hsl(0, 0%, 52%)',
        'theme-very-light-gray': 'hsl(0, 0%, 98%)'
      },
      fontFamily: {
        sans: 'Nunito Sans'
      }
    }
  },
  plugins: []
}
