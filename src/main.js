import './assets/main.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import { useCountriesStore } from './stores/countries'

import { library } from '@fortawesome/fontawesome-svg-core'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import {
  faMoon as fasMoon,
  faArrowLeftLong,
  faChevronDown,
  faMagnifyingGlass,
  faXmark
} from '@fortawesome/free-solid-svg-icons'

import { faMoon as farMoon } from '@fortawesome/free-regular-svg-icons'
import App from './App.vue'
import router from './router'

library.add(fasMoon, farMoon, faArrowLeftLong, faChevronDown, faMagnifyingGlass, faXmark)

const app = createApp(App)

app.use(createPinia())
app.component('FaIcon', FontAwesomeIcon)

const countriesStore = useCountriesStore()

Promise.all([countriesStore.getCountries()]).then(() => {
  app.use(router)
  app.mount('#app')
})
