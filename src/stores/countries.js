import { defineStore } from 'pinia'
import countries_local from './data.json'
export const useCountriesStore = defineStore('countries', {
  state: () => {
    return {
      countries: [],
      filterRegion: '',
      filterText: ''
    }
  },
  actions: {
    async getCountries() {
      await fetch('https://restcountries.com/v3.1/all')
        .then((response) => response.json())
        .then((data) => {
          this.countries = data
        })
        .catch(() => {
          this.countries = countries_local
        })
    }
  },
  getters: {
    getCountry(state) {
      return (cca3) => {
        return state.countries.filter((country) => country.cca3 === cca3)[0]
      }
    },
    getNameByCode(state) {
      return (cca3) => {
        return state.countries.filter((country) => country.cca3 === cca3)[0].name.official
      }
    },
    getRegionsList(state) {
      const regionsSet = new Set(state.countries.map((country) => country.region))
      const regionsList = [...regionsSet]
      regionsList.sort()
      return regionsList
    },
    filteredCountries(state) {
      let filtered = []
      if (state.filterRegion === '') {
        filtered = state.countries
      } else {
        filtered = state.countries.filter((country) => country.region === state.filterRegion)
      }

      if (state.filterText === '') {
        return filtered
      } else {
        return filtered.filter((country) =>
          country.name.official.toLowerCase().includes(state.filterText.toLowerCase())
        )
      }
    }
  }
})
